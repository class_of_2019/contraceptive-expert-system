###########################################################################################################################################
###                                                    Contracptive Use Advisor                                                         ###
###########################################################################################################################################

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    last modified: 
        January 2019
"""

import wx, wx.adv,clips, sys, json, re

""" 
    Global Variables
"""
APP_EXIT = wx.NewId()
ID_MENU_NEW = wx.NewId()


class ContraceptiveAdvisor(wx.Frame):
    """ 
        Main Class
    """

    def __init__(self, *args, **kwargs):
        """
            Init , calls functions in required order
        """
        super(ContraceptiveAdvisor, self).__init__(*args, **kwargs)

        # Intialize GUI 
        self.InitUI()

        # Get loads clips
        self.InitClips()


    def CreateMenuBar(self):
        """ 
            Function to create MenuBar
        """

        menubar = wx.MenuBar()

        fMenu = wx.Menu()
        hMenu =  wx.Menu()
        
        quit_menu_item = wx.MenuItem(fMenu, APP_EXIT, '&Quit\tCtrl+c')
        start_menu_item = wx.MenuItem(fMenu, ID_MENU_NEW, '&Start\tCtrl+N')
        
        fMenu.Append(start_menu_item)
        fMenu.Append(quit_menu_item)

        menubar.Append(fMenu, '&File')

        self.SetMenuBar(menubar)

        # Set listener for close events
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)

        self.Bind(wx.EVT_MENU, self.StartApp , id=ID_MENU_NEW)
        self.Bind(wx.EVT_MENU, self.OnCloseWindow, id=APP_EXIT)

    
    def OnCloseWindow(self, e):
        """
            Function to confirm application close
        """

        dial = wx.MessageDialog(None, 'Proceed?', 'Attention',
            wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)

        ret = dial.ShowModal()

        if ret == wx.ID_YES:
            self.Destroy()
        else:
            e.Veto()
    
    def ShowMessagePopUp(self,message,title):
        wx.MessageBox( message, title,wx.OK | wx.ICON_INFORMATION)


    def joinStrings(stringList):
        """
            Function to contatenate string
        """
        return ''.join(string for string in stringList)


    def StoreResponse(self, number ,question, answer):
        """
            Function to store responses entered by user
        """
        self.QuestionAndAnswers[number] = { question : answer }


    def UnpackResponse(self,type):
        """ 
            Function to unpack responses 
        """

        if (re.match(type,'string') ) :

            output = ''

            for k,v in self.QuestionAndAnswers.items():

                for k1,v1 in  v.items():
                    ques_ans= "Question #" + str(k) + " : " + str(k1) + " \nAnswer : "  + str(v1) + "\n\n"
            
                #output = self.joinStrings( output + ques_ans)

                output = output + ques_ans
            
            return output


    def StartApp(self,e):
        """
             Function to start ES (clips) module 
        """

        clips.Reset()
        clips.Run()
        
        self.nextUIState()
        
    
    def InitUI(self):
        """
            Function to handle UI Initialization
        """
        
        # Create MenuBar
        self.CreateMenuBar()
        
        self.panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.AddStretchSpacer()
        self.question = wx.Panel(self.panel, 1, name = "Question")
        sizer.Add(self.question,
                  flag = wx.ALL |
                         wx.EXPAND |
                         wx.GROW |
                         wx.ALIGN_CENTRE_VERTICAL |
                         wx.ALIGN_CENTRE_HORIZONTAL,
                  border = 10,
                  proportion = 1)
                  
        self.answers = wx.Panel(self.panel, -1, name = "Answers")
        sizer.Add(self.answers,
                  flag = wx.ALL |
                         wx.ALIGN_CENTRE,
                  border = 5,
                  proportion = 3)
                  
        self.buttons = wx.Panel(self.panel, -1, name = "Buttons")
        sizer.Add(self.buttons,
                  flag = wx.ALL |
                         wx.ALIGN_CENTRE,
                  border = 5,
                  proportion = 1)
                  
        self.panel.SetSizer(sizer)
        
        self.Maximize()
        self.SetTitle('Contraceptive Advisor Expertise System')
        self.Centre()
        
        
    def InitClips(self):
        """
            Function to load clips file and contraception details file
        """
        
        # Load clips
        clips.Load("./rules.clp")
        
        clips.Reset()
        clips.Run()

        # Initialize repsonse store

        self.QuestionAndAnswers= {}
        self.questionCount = 0

        # Load contraceoption explanation files
        with open('explanations.json') as e_file:
          self.explanations = json.load(e_file)
        
        # Load the questions
        self.nextUIState()
        
    
    def prepareExplanation(self, prompt, facts):
        """
            Function to prepare explanation
        """

        v_sizer = wx.BoxSizer(wx.VERTICAL) 

        # Obtain name for method
        method = prompt[25:]
        
        for (k, v) in self.explanations.items():


            if ( re.match (method, str(k) )):

                # Function to generate view

                for k1,v1 in v.items():
                    
                    font_b = wx.Font(12, wx.FONTFAMILY_DEFAULT,wx.FONTSTYLE_NORMAL,wx.FONTWEIGHT_BOLD )
                    font_n = wx.Font(10, wx.FONTFAMILY_DEFAULT,wx.FONTSTYLE_NORMAL,wx.FONTWEIGHT_NORMAL )


                    hbox1 = wx.BoxSizer(wx.HORIZONTAL) 

                    text_title = wx.StaticText(self.answers, -1, k1)
                    text_title.SetFont(font_b)
                    hbox1.Add(text_title , 1, wx.EXPAND|wx.ALIGN_LEFT|wx.ALL,5,1) 

                    text_content = wx.StaticText(self.answers, -1, v1 ) 
                    text_content.SetFont(font_n)
                    hbox1.Add(text_content, 1, wx.EXPAND|wx.ALIGN_LEFT|wx.ALL|wx.ST_ELLIPSIZE_END,5,4) 
            
                    v_sizer.Add(hbox1, flag = wx.ALL |
                         wx.EXPAND |
                         wx.GROW,
                  border = 5)
                
                method_e = False

                break

            else:
                # Method not found in explanation list

                method_e = True


        if method_e:
            print("Error contraception method : " + method +"  details cannot be found.\n Try reloading/updating resouce")
            #sys.exit()
        else:
            self.answers.SetSizer(v_sizer)


    def nextUIState(self):
        """
            Function to re-create the Dialog window to match the current state in Working Memory.
        """
        
        # Get the state-list.
        factlist = clips.Eval("(find-all-facts ((?f state-list)) TRUE)")
        if len(factlist) == 0:
            return
            
        currentID = factlist[0].Slots["current"]
        
        
        # Get the current UI state.
        factlist = clips.Eval("(find-all-facts ((?f UI-state)) (eq ?f:id %s))" % (currentID))
        if len(factlist) == 0:
            return
            
            
        # Clear the Buttons panel and re-create it for this UI state
        self.buttons.DestroyChildren()
        
        sizer = wx.BoxSizer(wx.HORIZONTAL)

        next = wx.Button(self.buttons, -1, "Next")
        responses = wx.Button(self.buttons, -1, "Explanation")
        
        sizer.Add(next, flag = wx.ALL, border = 5)
        sizer.Add(responses, flag = wx.ALL, border = 5)

        
        self.panel.Bind(wx.EVT_BUTTON, self.handleEvent, next)
        self.panel.Bind(wx.EVT_BUTTON, self.handleEvent, responses)

        
        self.buttons.SetSizer(sizer)
        
        self.buttons.Fit()
        
        # Determine the Next/Details button state.
        state = factlist[0].Slots["state"]
        if state == "final":
            next.SetLabel("Restart")
            next.Show(True)

            responses.Show(True)
            
        elif state == "initial":
            next.SetLabel("Next")
            next.Show(True)
            responses.Show(False)

            
        else:
            next.SetLabel("Next")
            next.Show(True)
            responses.Show(False)

            
        self.buttons.SetSizer(sizer)

        
        # Set the label to the display text.
        theText = factlist[0].Slots["prompt"]
        
        self.theText = theText

        self.question.DestroyChildren()
        
        font = wx.Font(20, wx.FONTFAMILY_DEFAULT,wx.FONTSTYLE_NORMAL,wx.FONTWEIGHT_BOLD )
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        
        prompt_q = wx.StaticText(self.question, -1,  style = wx.ALIGN_CENTRE)
        prompt_q.SetLabel(theText)
        prompt_q.SetFont(font)
        
        sizer.Add( prompt_q,
                  flag = wx.ALL |
                         wx.GROW |
                         wx.EXPAND,
                  border = 5,
                  proportion = 1 )
        self.question.SetSizer(sizer)
        self.question.Fit()


        # Set up the choices.
        valid_answers = factlist[0].Slots["valid-answers"]
        selected = factlist[0].Slots["response"]
        
        
        # Clear the Answers panel and re-create it as per the current UI state
        self.answers.DestroyChildren()
        
        

        # Display appropriate dsiplay items based on current execution state 
        if state == "final":

            # Get the state-list.
            factlist = clips.Eval("(find-all-facts ((?f state-list)) TRUE)")
            if len(factlist) == 0:
                return
        
            self.prepareExplanation(theText,factlist)
        
        else:

            sizer = wx.BoxSizer(wx.HORIZONTAL)

            self.answers._buttons = []
            for answer in valid_answers:
                r = wx.RadioButton(self.answers, -1, answer)
                if answer == selected:
                    r.SetValue(True)
                    
                sizer.Add(r, flag = wx.ALL, border = 5)
                self.answers._buttons.append(r)

            self.answers.SetSizer(sizer)

        self.answers.Fit()
        
        self.panel.Layout()
        self.panel.Refresh()

        
    def handleEvent(self, event):
        """
            Function that triggers the next state in Working Memory based on the wx.Button pressed and triggers a update of GUI.
        """
        
        # Get the state-list.
        factlist = clips.Eval("(find-all-facts ((?f state-list)) TRUE)")
        if len(factlist) == 0:
            return
            
        currentID = factlist[0].Slots["current"]

        # Handle button events.
        if event.GetEventObject().GetLabel() == "Next":
            if len(self.answers._buttons) == 0:
                clips.Assert("(next %s)" % (currentID))
                
            else:
                # Get current selection
                for b in self.answers._buttons:
                    if b.GetValue():
                        break
                
                # Add question count metric
            
                self.questionCount += 1

                self.StoreResponse( self.questionCount,self.theText, str(b.GetLabel()) )

                clips.Assert("(next %s %s)" % (currentID, str(b.GetLabel())))
    
        elif event.GetEventObject().GetLabel() == "Restart":

            clips.Reset()
        
        elif  event.GetEventObject().GetLabel() == "Explanation":
            # Display answers given by 
            
            # Get the current UI state.
            factlist = clips.Eval("(find-all-facts ((?f UI-state)) (eq ?f:id %s))" % (currentID))
            if len(factlist) == 0:
                return

            explaination = factlist[0].Slots["explain"]
            contraception = factlist[0].Slots["prompt"]

            responses = self.UnpackResponse('string')

            self.ShowMessagePopUp("Answers given :\n" + responses + " Rules in Expert System\n Conditions met:\n" + explaination ,'Explanation for suggestion')

            clips.Reset()
   
        clips.Run()
        
        self.nextUIState()
    

def main():
    """
         Function to run the application 
    """
    app = wx.App()
    ex = ContraceptiveAdvisor(None)
    ex.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()
