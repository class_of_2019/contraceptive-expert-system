; Contraceptive Advisor

; (deftemplate Breastfeeding (slot truth) (slot duration))
; (deftemplate Cirrhosis (slot had) (slot condition))
; (deftemplate contraseptive-use (slot used))
; (deftemplate wearable (slot wear))
; (deftemplate STIPrep (slot Prep))
; (deftemplate effectiveness (slot effectiveness))
; (deftemplate cost (slot cost))


; Age range
(defglobal ?*age_range_1* = "<18")
(defglobal ?*age_range_2* = "18-25")
(defglobal ?*age_range_3* = "25-30")
(defglobal ?*age_range_4* = "30-40")
(defglobal ?*age_range_5* = "40-50")
(defglobal ?*age_range_6* = ">50")

; Desired duration to be sterile
(defglobal ?*duration_1* = "<1_month")
(defglobal ?*duration_2* = "1_to_12_months")
(defglobal ?*duration_3* = "1_to_10_years")
(defglobal ?*duration_4* = "permanent")

; Frequency of administering contraception
(defglobal ?*frequency_1* = "daily")
(defglobal ?*frequency_2* = "weekly")
(defglobal ?*frequency_3* = "monthly")
(defglobal ?*frequency_4* = "one time")

; Genders
(defglobal ?*gender_1* = "male")
(defglobal ?*gender_2* = "female")

; Long acting
(defglobal ?*contra_1* = "Copper Intra-uterine device") ; Also in emergency
(defglobal ?*contra_2* = "Implant")

; Hormonal
(defglobal ?*contra_3* = "Combined oral pill")
(defglobal ?*contra_4* = "Progestogen-only pill")
(defglobal ?*contra_5* = "vaginal ring")
(defglobal ?*contra_6* = "contraceptive patch")
(defglobal ?*contra_7* = "Intrauterine system (IUS)")
(defglobal ?*contra_8* = "Contraceptive injection")

; Barrier
(defglobal ?*contra_9* = "Condom") ; male
(defglobal ?*contra_9_f* = "Condom") ; female
(defglobal ?*contra_10* = "Internal condom") ; caps, barriers

; Emergency
(defglobal ?*contra_11* = "Emergency contraceptive pill")

; Natural
(defglobal ?*contra_12* = "Fertility awareness")

; Permanent
(defglobal ?*contra_13* = "Vasectomy")
(defglobal ?*contra_14* = "Tubal ligation")

; Illnesses
(defglobal ?*illness_1* = "Anemia")
(defglobal ?*illness_2* = "Bariatric_surgery")
(defglobal ?*illness_3* = "Breast_cancer")
(defglobal ?*illness_4* = "Breast_problems-benign")
(defglobal ?*illness_5* = "Cervical_cancer")
(defglobal ?*illness_6* = "Depression")
(defglobal ?*illness_7* = "Diabetes Mellitus")


(deftemplate UI-state
   (slot id (
       default-dynamic (gensym*)
       )
    )
   (slot prompt)
   (slot explain(
       default none)
   )
   (slot relation-asserted (
       default none)
    )
   (slot response (
       default none)
   )
   (multislot valid-answers)
   (slot state (
       default middle
       )
    )
)

(deftemplate state-list
    (slot current)
    (multislot sequence)
)

(deffacts startup
    (state-list)
)


; Startup rule
(defrule Startup ""
    =>
    (assert (UI-state
        (prompt "Welcome to the Contraception Advisor Expert System.")
        (relation-asserted start)
        (state initial)
        (valid-answers)
        )
    )
)


; Query rules
(defrule Get_gender ""
    (logical (start))
    =>
    (assert (UI-state
        (prompt "Specify your gender?")
        (relation-asserted gender)
        (response ?*gender_1*)
        (valid-answers ?*gender_1* ?*gender_2*)
        )
    )
)

(defrule Get_age ""
    (or
        (logical (gender male))
        (logical (gender female))
    )
    => 
    (assert (UI-state
        (prompt "Specify your age group?")
        (relation-asserted age)
        (response ?*age_range_6*)
        (valid-answers ?*age_range_1* ?*age_range_2* ?*age_range_3* ?*age_range_4* ?*age_range_5* ?*age_range_6*)
        )
    )
)

(defrule Get_situation ""
    (or
        (logical (age <18))
        (logical (age 18-25))
        (logical (age 25-30))
        (logical (age 30-40))
        (logical (age 40-50))
        (logical (age >50))
    )
    (logical (gender female))
     =>
    (assert (UI-state
        (prompt "Is it an emergency situation?")
        (relation-asserted situation)
        (response No)
        (valid-answers No Yes)
        )
    )
)

(defrule Get_duration ""
    (logical (gender male))
    (or
        (logical (age <18))
        (logical (age 18-25))
        (logical (age 25-30))
        (logical (age 30-40))
        (logical (age 40-50))
        (logical (age >50))
    )
    =>
     (assert (UI-state
        (prompt "Specify the duration to be sterile?")
        (relation-asserted duration)
        (response ?*duration_4*)
        (valid-answers ?*duration_1* ?*duration_4*)
        )
    )
)

(defrule Get_duration_f ""
    (logical (gender female))
    (logical (situation No))
    (or
        (logical (age <18))
        (logical (age 18-25))
        (logical (age 25-30))
        (logical (age 30-40))
        (logical (age 40-50))
        (logical (age >50))
    )
    =>
     (assert (UI-state
        (prompt "Specify the duration to be sterile?")
        (relation-asserted duration)
        (response ?*duration_4*)
        (valid-answers ?*duration_1* ?*duration_2* ?*duration_3* ?*duration_4*)
        )
    )
)

(defrule Get_frequency ""
    (logical (gender female))
    (logical (situation No))
    (logical (duration !permanent))
    (or 
        (logical (duration <_1_month))
        (logical (duration 1_to_12_months))
        (logical (duration 1_to_10_years))
        (logical (duration permanent))
    )
    (logical (situation No))
    =>
     (assert (UI-state
        (prompt "Specify the frequency of administering contraception?")
        (relation-asserted frequency)
        (response ?*frequency_4*)
        (valid-answers ?*frequency_1* ?*frequency_2* ?*frequency_3* ?*frequency_4*)
        )
    )
)

(defrule Get_illness ""
    (logical ( gender female))
    (logical (situation No))
    =>
    (assert (UI-state
        (prompt "Do you have any of the listed illnesses?")
        (relation-asserted illness)
        (response No)
        (valid-answers No ?*illness_1* ?*illness_2* ?*illness_3* ?*illness_4* ?*illness_5* ?*illness_6* ?*illness_7*)
        )
    )
)


; Contraception advice rules
(defrule IUD
    (logical (gender female))
    (or
        (logical (age <18))
        (logical (age 18-25))
        (logical (age 25-30))
        (logical (age 30-40))
        (logical (age 40-50))
        (logical (age >50))
    )
    (logical (duration 1_to_10_years))
    (or
        (logical (illness Anemia))
        (logical (illness Bariatric_surgery))
        (logical (illness Breast_cancer))
        (logical (illness Depression))
        (logical (illness Cervical_cancer))
        (logical (illness Diabetes_Mellitus))
        (logical (illness No))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive:"  ?*contra_1*))
        (explain "Gender : females  Age : all   Duration : 1 - 10 years Health Conditions : Anemia, Bariatric Surgery, Breast Cancer, Depression Cervical Cancer, Diabetes Mellitus")
        (state final)
        )
    )
)

(defrule IUD_emergency
    (logical (situation Yes))
    (logical (gender female))
    (or
        (logical (age <18))
        (logical (age 18-25))
        (logical (age 25-30))
        (logical (age 30-40))
        (logical (age 40-50))
        (logical (age >50))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_1*))
        (explain "Gender : female  Age : all  Emergency : yes")
        (state final)
        )
    )
)

(defrule Implant ""
    (logical (situation No))
    (logical (gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical ( duration 1_to_10_years))
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_2*))
        (explain "Gender : female  Age : all  Emergency : no Duration : 1 - 10 years")
        (state final)
        )
    )
)

(defrule COP ""
    (logical (situation No))
    (logical (gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical (duration <1_month))
    (or
        (logical (illness Anemia))
        (logical (illness Bariatric_surgery))
        (logical (illness Breast_problems-benign))
        (logical (illness Depression))
        (logical (illness Cervical_cancer))
        (logical (illness Diabetes_Mellitus))
        (logical (illness No))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_3*))
        (explain "Gender : female  Age : all Emergency : no  Duration : < 1 month Health Conditions : Anemia, Bariatric Surgery, Breast_problems-benign, Depression Cervical Cancer, Diabetes Mellitus")
        (state final)
        )
    )
)

(defrule POP ""
    (logical (situation No))
    (logical (gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical (duration <1_month))
    (or
        (logical ( illness Anemia))
        (logical ( illness Bariatric_surgery))
        (logical ( illness Breast_problems-benign))
        (logical ( illness Depression))
        (logical ( illness Cervical_cancer))
        (logical ( illness Diabetes Mellitus))
        (logical (illness No))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_4*))
        (explain "Gender : female  Age : all Emergency : no  Duration : < 1 month Health Conditions : Anemia, Bariatric Surgery, Breast_problems-benign, Depression Cervical Cancer, Diabetes Mellitus")
        (state final)
        )
    )
)

(defrule Vaginal_ring ""
    (logical (situation No))
    (logical (gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (or
        (logical ( illness Anemia))
        (logical ( illness Bariatric_surgery))
        (logical ( illness Breast_problems-benign))
        (logical ( illness Depression))
        (logical ( illness Cervical_cancer))
        (logical ( illness Diabetes_Mellitus))
        (logical (illness No))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_5*))
        (explain "Gender : female  Age : all Emergency : no  Health Conditions : Anemia, Bariatric Surgery, Breast_problems-benign, Depression Cervical Cancer, Diabetes Mellitus")
        (state final)
        )
    )
)

(defrule Contraceptive_patch ""
    (logical (situation No))
    (logical ( gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (or
        (logical ( illness Anemia))
        (logical ( illness Bariatric_surgery))
        (logical ( illness Breast_problems-benign))
        (logical ( illness Depression))
        (logical ( illness Cervical_cancer))
        (logical ( illness Diabetes_Mellitus))
        (logical (illness No))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_6*))
        (explain "Gender : female  Age : all Emergency : no  Duration : < 1 month Health Conditions : Anemia, Bariatric Surgery, Breast_problems-benign, Depression Cervical Cancer, Diabetes Mellitus")
        (state final)
        )
    )
)

(defrule IUS ""
    (logical (situation No))
    (logical ( gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical ( duration 1_to_12_months))
    (or
        (logical ( illness Anemia))
        (logical ( illness Bariatric_surgery))
        (logical ( illness Breast_problems-benign))
        (logical ( illness Depression))
        (logical ( illness Diabetes_Mellitus))
        (logical (illness No))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_7*))
        (explain "Gender : female  Age : all Emergency : no  Duration : 1 - 12 months Health Conditions : Anemia, Bariatric Surgery, Breast_problems-benign, Depression Cervical Cancer, Diabetes Mellitus")
        (state final)
        )
    )
)

(defrule Contraceptive_injection ""
    (logical (situation No))
    (logical (gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical ( duration 1_to_12_months))
    (or
        (logical ( illness Anemia))
        (logical ( illness Bariatric_surgery))
        (logical ( illness Breast_problems-benign))
        (logical ( illness Depression))
        (logical ( illness Cervical_cancer))
        (logical ( illness Diabetes_Mellitus))
        (logical (illness No))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_8*))
        (explain "Gender : female  Age : all Emergency : no  Duration : 1 - 12  month Health Conditions : Anemia, Bariatric Surgery, Breast_problems-benign, Depression Cervical Cancer, Diabetes Mellitus")
        (state final)
        )
    )
)

(defrule Condom ""
    (logical (gender male))
    (or
        (logical (age <18))
        (logical (age 18-25))
        (logical (age 25-30))
        (logical (age 30-40))
        (logical (age 40-50))
        (logical (age >50))
    )
    (logical ( duration  <1_month))
    (logical (frequency "daily"))
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_9*))
        (explain "Gender : male  Age : all Emergency : no")
        (state final)
        )
    )
)

(defrule Condom_f ""
    (logical (situation No))
    (logical (gender female))
    (or
        (logical (age <18))
        (logical (age 18-25))
        (logical (age 25-30))
        (logical (age 30-40))
        (logical (age 40-50))
        (logical (age >50))
    )
    (logical ( duration  <1_month))
    (logical (frequency "daily"))
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_9_f*))
        (explain "Gender : female  Age : all Emergency : no")
        (state final)
        )
    )
)

(defrule Internal_condom ""
    (logical (situation No))
    (logical ( gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical ( duration  <1_month))
    (logical (frequency "daily"))
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_10*))
        (explain "Gender : female  Age : all Emergency : no ")
        (state final)
        )
    )
)

(defrule Emergency_pill ""
    (logical (situation yes))
    (logical ( gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_11*))
        (explain "Gender : female  Age : all Emergency : yes  Duration : < 1 month Frequency : one time")
        (state final)
        )
    )
)

(defrule Awareness
    (logical (situation No))
    (logical ( gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_12*))
        (explain "Gender : female  Age : all Emergency : no ")
        (state final)
        )
    )
)

(defrule Vasectomy ""
    (logical (gender male))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical ( duration permanent))
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_13*))
        (explain "Gender : male  Age : all Emergency : no  Duration : permanent ")
        (state final)
        )
    )
)

(defrule Tubal_ligation ""
    (logical (situation No))
    (logical (gender female))
    (or
        (logical ( age <18))
        (logical ( age 18-25))
        (logical ( age 25-30))
        (logical ( age 30-40))
        (logical ( age 40-50))
        (logical ( age >50))
    )
    (logical ( duration permanent))
    =>
    (assert (UI-state
        (prompt (str-cat "Suggested contraceptive: " ?*contra_14*))
        (explain "Gender : female  Age : all Emergency : no  Duration : permanent")
        (state final)
        )
    )
)

; GUI interaction
(defrule ask-question
	(declare (salience 5))
	(UI-state (id ?id))
	?f <- (state-list
		(sequence $?s&:
		(not (member$ ?id ?s)))
	)
	=>
	(modify ?f
		(current ?id)
		(sequence ?id ?s)
	)
	(halt)
)

(defrule handle-next-no-change-none-middle-of-chain
	(declare (salience 10))
	?f1 <- (next ?id)
   	?f2 <- (state-list
   		(current ?id)
   		(sequence $? ?nid ?id $?)
   	)
   	=>
   	(retract ?f1)
	(modify ?f2 (current ?nid))
	(halt)
)

(defrule handle-next-response-none-end-of-chain
   	(declare (salience 10))
   	?f <- (next ?id)
   	(state-list
   		(sequence ?id $?)
   	)
   	(UI-state
   		(id ?id)
   		(relation-asserted ?relation)
   	)
   	=>
   	(retract ?f)
   	(assert (add-response ?id))
)

(defrule handle-next-no-change-middle-of-chain
   	(declare (salience 10))
   	?f1 <- (next ?id ?response)
   	?f2 <- (state-list
   		(current ?id)
   		(sequence $? ?nid ?id $?)
   	)
   (UI-state
   		(id ?id)
   		(response ?response)
   )
   =>
   (retract ?f1)
   (modify ?f2 (current ?nid))
   (halt)
)

(defrule handle-next-change-middle-of-chain
   	(declare (salience 10))
   	(next ?id ?response)
   	?f1 <- (state-list
   		(current ?id)
   		(sequence ?nid $?b ?id $?e)
   	)
  	(UI-state
   		(id ?id)
   		(response ~?response)
   	)
   ?f2 <- (UI-state (id ?nid))
   =>
   (modify ?f1 (sequence ?b ?id ?e))
   (retract ?f2)
)

(defrule handle-next-response-end-of-chain
	(declare (salience 10))
	?f1 <- (next ?id ?response)
	(state-list (sequence ?id $?))
	?f2 <- (UI-state
		(id ?id)
	    (response ?expected)
        (relation-asserted ?relation)
    )
	=>
	(retract ?f1)
	(if
		(neq ?response ?expected)
	then
		(modify ?f2 (response ?response))
	)
	(assert (add-response ?id ?response))
)

(defrule handle-add-response
    (declare (salience 10))
    (logical (UI-state
   		(id ?id)
        (relation-asserted ?relation))
  	)
    ?f1 <- (add-response ?id ?response)
    =>
    (str-assert (str-cat "(" ?relation " " ?response ")"))
    (retract ?f1)
)

(defrule handle-add-response-none
   (declare (salience 10))
   (logical (UI-state
   		(id ?id)
	    (relation-asserted ?relation))
  	)
   ?f1 <- (add-response ?id)
   =>
   (str-assert (str-cat "(" ?relation ")"))
   (retract ?f1)
)
