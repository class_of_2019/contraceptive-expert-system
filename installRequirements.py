import os , sys 

def per_platform():

    platforms = {
        'linux' : 'Linux',
        'darwin' : 'OS X',
        'win32' : 'Windows'
    }

    if sys.platform not in platforms:
        return sys.platform
    
    platform = platforms[sys.platform]


    if  (platform == platforms['linux']):
        print ("[+] Running Linux ");

        print ("[+] Installing wxPython with pip ");

        os.system( "pip install -U -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-16.04 wxPython ")

        print ("[+] Cloning pyclips ");
        
        os.system( "sudo pip install -U  svn")


        installPyClips()

        
    elif (platform ==  platforms['darwin']):
        print ("[+] Running Mac ");

        print ("[+] Installing wxPython with pip ");
        os.system( "pip install -U wxPython svn")

        print ("[+] Cloning pyclips ");

        installPyClips()


    elif (platform ==  platforms['win32']):
        print ("[+] Running Windows ");

        print ("[+] Installing wxPython with pip ");
        os.system( "runas /noprofile /user:Administrator pip install -U wxPython svn")

        installPyClips()


    else:
        print("Not found");

            
def installPyClips():

    print ("[+] Cloning pyclips")

    import  svn.remote

    r = svn.remote.RemoteClient('https://svn.code.sf.net/p/pyclips/code/')
    r.checkout('pyclips-code')

    r =  None


    print ("[+] Installing pyclips");

    os.system( "python pyclips-code/pyclips/branches/pyclips-2.0/setup.py build")

    os.system( "python pyclips-code/pyclips/branches/pyclips-2.0/setup.py install")


if __name__ == '__main__':

    per_platform()
   