# Introduction

Program is an Expert System written in clips with a python front-end WxPython.

Its purpose is to advice on the most appropriate contraception method for use based on current premise.

## Overview
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/pdbuO1IRkvY/0.jpg)](https://www.youtube.com/watch?v=pdbuO1IRkvY)


## Installation

__Ensure python 2 is installed in system.__

Run the installRequirements.py to install

```shell
python2 installRequirements.py
```

This script will install python dependency packages for the project and also pyclips.



## Running

```
python2 contraceptive.wx.py
```



Tested on:

+ Arch => March 2019: working,
+ and Ubuntu  => March 2019: after slight configuration details below



## Extra Notes

For Ubuntu additional configuration may be required to install project dependencies.

One of the packages the  project depends on to build it is deprecated and thus  may require altering package manager sources in order to install it prior to running the installRequirements script.

